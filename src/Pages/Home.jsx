import React from 'react'
import Header from '../Components/Header'
import Main from '../Components/Main'

export const Home = () => {
  return (
    <div>
        <h1 className='search_title'>Todo Search</h1>

        {/* component berisi search dan button addnewtask */}
        <Header />

        {/* component berisi filter */}
        <Main />
    </div>
  )
}
