import React, { useState } from 'react'
import  { EditRounded, DeleteRounded } from '@mui/icons-material';
import { Button, Checkbox } from '@mui/material';

function Input() {

  //buat state variabel untuk seluruh task
  const [tasks, setTasks] = useState([]);

  //buat state variabel untuk inputan terbaru yg baru dimasukan
  const [currInput, setCurrInput] = useState('');
  
  //function ketika tombol button di klik untuk melakukan submit
  const handleSubmit = (e) => {
      //preventDefault => untuk mencegah browser memuat ulang/refresh
      e.preventDefault();
      if (currInput.trim()) {
          setTasks([...tasks, { id: Date.now(), text: currInput }]);
          setCurrInput('');
      }
  };

  //function untuk melakukan delete ketika icon trash di klik
  const handleDelete = (taskId) => {
      setTasks(tasks.filter((task) => task.id !== taskId));
  };

  //function untuk melakukan edit ketika icon trash di klik
  const handleEdit = (taskId, newText) => {
      const updatedTasks = tasks.map((task) =>
          task.id === taskId ? { ...task, text: newText } : task
      );
      setTasks(updatedTasks);
  };


  return (
      <div className='Input'>
        <h1 id='input_title'>Todo Input</h1>
        <div className='input_container'>
          {/* form melakukan function submit */}
          <form onSubmit={handleSubmit}>
              <input
                  type='text'
                  value={currInput}
                  onChange={e => setCurrInput(e.target.value)}
                  placeholder='Masukkan input disini...'
              />
              <button type='submit'>Submit</button>
          </form>
        </div>
        <ul>
          <h1 id='list_title'>Todo List</h1>
            {tasks.map((task) => (
              <li key={task.id} >
                  <div className='task_container'>
                    <span>{task.text}</span>
                    <div className='task_icon'>
                      <Checkbox
                        checked={task.complete} />
                      <button
                        type='submit'
                        onClick={() => handleDelete(task.id)}>
                        <DeleteRounded />
                      </button>
                      <button
                        type='submit' 
                        onClick={() => {
                        // edit task menggunakan comand prompt
                        const newText = prompt('Edit task', task.text);
                        if (newText !== null) {
                            handleEdit(task.id, newText);
                        }
                        }}
                      >
                        <EditRounded />
                      </button>
                    </div>
                </div>
              </li>
            ))}
          </ul>
          <div className='button_delete'>
            <Button variant='contained' color='error' onClick={() => setTasks([])}>Delete all task</Button>
          </div>
      </div>
  );
}


export default Input