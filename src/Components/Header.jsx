import React from 'react'
import { useNavigate } from "react-router-dom"
import { Button } from '@mui/material'
import { SearchRounded } from '@mui/icons-material';

function Header() {

  //membuat function untuk mengarahkan button ke halaman todo menggunakan useNavigate
  const navigate = useNavigate()

  return (
    <div className='Header'>
      <div className='Search'>
        <form>
          <Button className="search_icon">
            <SearchRounded />
          </Button>
          <input placeholder='Search Todo' />
        </form>
        <Button className="search_button" variant="contained">Search</Button>
      </div>
      <div className='newTask'>
        <Button variant="contained" onClick={() => navigate('/todo')}>Add New Task</Button>
      </div>
    </div>
  )
}

export default Header