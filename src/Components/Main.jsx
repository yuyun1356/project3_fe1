import  { DeleteRounded } from '@mui/icons-material';
import { Button, Checkbox } from '@mui/material';
import React, { useState, useEffect } from 'react'
import data from '../data_awal/data.json'

function Main() {

    //buat state variabel untuk ambil task dari data json
    const [tasks, setTasks] = useState([]);
    useEffect(() => {
        setTasks(data);
    }, []);

    //function untuk melakukan delete ketika icon trash di klik
    const handleDelete = (id) => {
    setTasks(tasks.filter((task) => task.id !== id));
    };

    //filter all
    const handleAll = () => {
        setTasks(data);
    };

    //filter done
    const handleDone = () => {
        const filterdone = tasks.filter((task) => task.complete === true);
        setTasks(filterdone)
    };

    //filter todo
    const handleTodo = () => {
        const filtertodo = tasks.filter((task) => task.complete === false);
        setTasks(filtertodo);
    };


  return (
    <div className='Main'>
        <h1>Filter TodoList</h1>
        <div className='filter_container'>
            <Button variant='contained' onClick={() => handleAll()}>All</Button>
            <Button variant='contained' onClick={() => handleDone()}>Done</Button>
            <Button variant='contained' onClick={() => handleTodo()}>Todo</Button>
        </div>

        <ul>
            {tasks.map((task) => (
              <li key={task.id} >
                  <div className='task_container'>
                    <span>{task.task}</span>
                    <div className='task_icon'>
                      <Checkbox
                        checked={task.complete} />
                      <button
                        type='submit'
                        onClick={() => handleDelete(task.id)}>
                        <DeleteRounded />
                      </button>
                    </div>
                </div>
              </li>
            ))}
          </ul>
          <div className='button_delete'>
            <Button variant='contained' color='error' onClick={() => setTasks([])}>Delete all task</Button>
          </div>
    </div>
  )
}

export default Main