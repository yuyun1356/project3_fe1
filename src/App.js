import { BrowserRouter as Router, Routes, Route } from 'react-router-dom'
import './App.css';
import { Home } from './Pages/Home';
import { Todo } from './Pages/Todo';

function App() {
  return (
    <Router>
      <Routes>
        {/* Terdapat 2 halaman, halaman home (halaman awal) & todo (melakukan input/edit todo) */}
        <Route path='/' element={<Home />}></Route>
        <Route path='/todo' element={<Todo />}></Route>
      </Routes>
    </Router>
  );
}

export default App;
